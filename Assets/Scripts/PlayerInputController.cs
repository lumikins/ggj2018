﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerInputController : MonoBehaviour {

    public EventSystem eventSystem;
    public ConnectionManager connectionManager;

    private GameObject firstChoice;
    private GameObject secondChoice;

    // show messages to player
    public GameStatusTextManager gameStatusTextManager;

	// raycast detectors (used for right clicks to delete links)
    private Ray ray;
	private RaycastHit hit;

    public void Choose()
    {

        if (firstChoice == null)
        {
            firstChoice = eventSystem.currentSelectedGameObject;
            Debug.Log("PlayerInputController: firstChoice = " + firstChoice);

			// peel off any excess characters in object name when displaying to the player
			int parenLocation = -1;
			// find the first occurence of a '(' character
			parenLocation = firstChoice.name.IndexOf("(");

			if(parenLocation != 0 || parenLocation != -1)
			{
				// no ( character found - show full name
				gameStatusTextManager.ShowPersistentText("Selected " + firstChoice.name);
			}
			else
			{
				// ( character found - strip off portion starting from ( and beyond
				gameStatusTextManager.ShowPersistentText("Selected " + firstChoice.name.Substring(0, parenLocation));
			}
        }
        else
        {
            secondChoice = eventSystem.currentSelectedGameObject;
            Debug.Log("PlayerInputController: secondChoice = " + secondChoice);
        }
    }

    private void Update()
    {
        // two distinct selections detected - attempt a connection
        if (firstChoice != null && secondChoice != null)
        {
            var start = firstChoice.GetComponent<Connectable>();
            var finish = secondChoice.GetComponent<Connectable>();

            if (start.ConnectTo(finish))
            {
            	// check if existing connection exists
            	if(connectionManager.CheckForExistingConnection(start, finish))
            	{
            		//Debug.Log("CONNECTION EXISTS!!!");
            		gameStatusTextManager.ShowInfoText("The connection you tried already exists.");
            	}
				else if(firstChoice == secondChoice)
				{
					// disallow connections with self (ie connecting a battery to itself if it has both electrical input and output)
				}
				else
            	{
        			// new connection - add to list
					connectionManager.MakeConnection(start, finish);
            	}
            }
            else
            {
				// no output/input match found
				gameStatusTextManager.ShowInfoTextTimed("These two objects don't connect in the order attempted.", 1.0f);
            }

			ClearChoices();
        }

		// Detect right clicks on objects with colliders by using raycasts
		// Right clicks are used to delete links (which can show up anywhere on the game board)
		if(Input.GetMouseButtonUp(1))
		{
			// we right clicked and let go
			// draw a ray from camera to the game board
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if(Physics.Raycast(ray, out hit))
		    {
		    	// we hit a box collider (only the connectors have box colliders in this game)
		    	Debug.Log("HIT:" + hit.collider.name);
		    	connectionManager.DeleteConnection(hit.collider.GetComponentInParent<ConnectionInfo>());
		    }

		}
			



    }

    void ClearChoices()
    {
        eventSystem.SetSelectedGameObject(null);
        firstChoice = null;
        secondChoice = null;        
    }

}
