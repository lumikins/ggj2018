﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// This class shows announcements on the status text field
public class GameStatusTextManager : MonoBehaviour {

	// NOT USING THE QUEUE ATM - rather have instant response vs lagged responses
	// Testing message queue
	public List<string> queuedMessages = new List<string>();
	// to get specific times a seperate array with times
	public List<float> messageTimes = new List<float>();

	// length to show a msg
	public float timeToDisplayMsg = .75f;

    public Text scoreText;
    public Text timerText;
    public Transform victoryPanel;
    public Text victoryText;


    private int score = 0;
    private float timer = 0;
    private float roundTime = 0f;


    private void Start()
    {

    }

    public void Victory()
    {
        victoryPanel.gameObject.SetActive(true);
        victoryText.text = "It took you " + Mathf.Round(roundTime).ToString() + " seconds to finish the level with " + score + " points.";
    }

    // this bool is true while a message is being shown in another coroutine
    // false when that message is done
    //bool messageCurrentlyShown;

    // Adds score to current score
    public void AddScore(int _score)
    {
        scoreText.text = "Score: " + (score += _score).ToString();
    }

    private void Update()
    {
        roundTime += Time.deltaTime;
        timerText.text = "Time spent: " + Mathf.Round(roundTime).ToString(); 
    }



    //	void Update()
    //	{
    //		
    //
    //		if(messageCurrentlyShown == false && queuedMessages.Count > 0)
    //		{
    //			// take the next item off the message queue and display it
    //			// always the first element (assuming lists add to the end)
    //
    //			if(messageTimes[0] == timeToDisplayMsg)
    //			{
    //				// default case
    //				StartCoroutine(ShowInfoTextCor(queuedMessages[0]));
    //			}
    //			else
    //			{
    //				// special timing given
    //				StartCoroutine(ShowInfoTextTimedCor(queuedMessages[0], messageTimes[0]));
    //			}
    //
    //
    //
    //			queuedMessages.RemoveAt(0);
    //			messageTimes.RemoveAt(0);
    //		}
    //	}



    // info text shows for 2 seconds
    public void ShowInfoText(string theText)
    {
		StartCoroutine(ShowInfoTextCor(theText));
//		queuedMessages.Add(theText);
//		messageTimes.Add(timeToDisplayMsg);
    }

    // show text for a certain # of secs
	public void ShowInfoTextTimed(string theText, float numSecs)
    {
		StartCoroutine(ShowInfoTextTimedCor(theText, numSecs));
//		queuedMessages.Add(theText);
//		messageTimes.Add(numSecs);
    }

    // text is shown and not replaced until something else comes up
    public void ShowPersistentText(string theText)
    {
		GameObject.Find("Status Text").GetComponent<Text>().text = theText;
    }

    IEnumerator ShowInfoTextCor(string theText)
    {
    	//messageCurrentlyShown = true;

        GameObject.Find("Status Text").GetComponent<Text>().text = theText;

		yield return new WaitForSeconds(timeToDisplayMsg);

		GameObject.Find("Status Text").GetComponent<Text>().text = "...";

		//messageCurrentlyShown = false;

    }

	IEnumerator ShowInfoTextTimedCor(string theText, float timeDelay)
    {
    	//messageCurrentlyShown = true;

        GameObject.Find("Status Text").GetComponent<Text>().text = theText;

		yield return new WaitForSeconds(timeDelay);

		GameObject.Find("Status Text").GetComponent<Text>().text = "...";

		//messageCurrentlyShown = false;

    }



}
