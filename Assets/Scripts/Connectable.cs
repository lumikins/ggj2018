using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Connectable : MonoBehaviour {

	// list of possible input/output connections for this connectable
    public List<Connector> inputs = new List<Connector>();
    public List<Connector> outputs = new List<Connector>();

	// Gameobject that will represent the connection from one connectable to another
	public ConnectionInfo connectorPrefab;

	// boolean states to keep track of which connectables are our start and finish points
	// defaults to false - change to true in inspector manually 
	// valid level needs 1 start obj and 1 finish obj
	public bool isStartObject = false;
	public bool isFinishObject = false;

    // references to children UI panels for displaying connection icons
    public Transform inputPanel;
    public Transform outputPanel;

    ConnectionManager connectionManager;

    private void Awake()
    {
        connectionManager = FindObjectOfType<ConnectionManager>();
    }

    private void Start()
    {
        DisplayConnectionIcons();
        HighlightGoals();

    }

    void HighlightGoals()
    {
        if (isStartObject)
        {
            GetComponentInChildren<Image>().color = new Color(0, 255, 0, 35);
            GetComponentInChildren<Text>().text = "START HERE";
        }

        if (isFinishObject)
        {
            GetComponentInChildren<Image>().color = new Color(255, 0, 0, 35);
            GetComponentInChildren<Text>().text = "FINISH HERE";
        }
    }


    // Displays all possible connection type icons on a Connectable
    void DisplayConnectionIcons()
    {
        var inputPrefabs = new List<GameObject>();
        var outputPrefabs = new List<GameObject>();
        var possibleIcons = connectionManager.connectorIcons;

        foreach (GameObject possibleIcon in possibleIcons)
        {
            var connectorType = possibleIcon.GetComponent<ConnectorIcon>().connector;
            if (inputs.Contains(connectorType))
                inputPrefabs.Add(possibleIcon);

            if (outputs.Contains(connectorType))
                outputPrefabs.Add(possibleIcon);
        }

        foreach (GameObject prefab in inputPrefabs)
        {
            var clone = Instantiate(prefab, inputPanel);
        }

        foreach (GameObject prefab in outputPrefabs)
        {
            var clone = Instantiate(prefab, outputPanel);
        }
    }

    public bool ConnectTo(Connectable targetObject)
    {
        foreach (Connector output in outputs)
        {
            if (targetObject.hasConnector(output))
            {
                if (targetObject.GetComponent<Animator>() != null)
                    targetObject.GetComponent<Animator>().SetBool("isConnected", true);

                if (targetObject.GetComponent<AudioSource>() != null)
                    targetObject.GetComponent<AudioSource>().Play();

                return true;
            }
                
        }
        
    return false;
    }

    public bool hasConnector(Connector inputConnector)
    {
        foreach (Connector connector in inputs)
        {
            if (connector == inputConnector)
                return true;
        }

        return false;
    }

    // grabs the first valid connection type between 2 connectables
    public Connector GetConnectionType(Connectable targetObject)
    {
		foreach (Connector output in outputs)
        {
            if (targetObject.hasConnector(output))
                return output;
        }

        return Connector.NULL;
    }
}

public enum Connector
{
    Electricity,
    Audio,
    Data,
    Kinetic,
    Heat,
    NULL
}
