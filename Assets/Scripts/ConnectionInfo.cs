﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectionInfo : MonoBehaviour {

	// stores information on what objects are connected and the type of connection made
	public Connectable start;
	public Connectable finish;
	public Connector connectionType;

	// reference to the line renderer that represents this connection
	public LineRenderer lineGraphic;

	// used for search algorhitms - true if it has been visited false otherwise
	public bool linkTraversed;

    public bool Contains(Connector connector)
    {
        return connector == connectionType;
    }
}
