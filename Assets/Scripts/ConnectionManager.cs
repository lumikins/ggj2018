using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConnectionManager : MonoBehaviour {

    public List<GameObject> connectorIcons = new List<GameObject>();

	// List of all connectors from one connectable to another
	public List<ConnectionInfo> connectorsList = new List<ConnectionInfo>();

	// show status text if certain things happen
	public GameStatusTextManager gameStatusTextManager;


    public void MakeConnection(Connectable start, Connectable finish)
    {
        // we will be connecting first choice to second choice 

        // instansiate the connector obj
        ConnectionInfo theConnection = Instantiate(start.connectorPrefab);

        // get it's Line Renderer component
        LineRenderer theLine = theConnection.GetComponent<LineRenderer>();

        // set how many vertex points this line will have
        theLine.positionCount = 2;

        // start of the line (first obj reference)
        theLine.SetPosition(0, start.transform.position);

        // end of the line (second obj reference)
        theLine.SetPosition(1, finish.transform.position);

        // width at line start
        theLine.startWidth = .6f;

        // width at line end
        theLine.endWidth = .1f;

        // line is drawn - now save information about this connection
        theConnection.start = start;
        theConnection.finish = finish;
        theConnection.connectionType = start.GetConnectionType(finish);
        theConnection.lineGraphic = theLine;

        // add a collider to this line
		AddColliderToLine(theLine, start.transform.position, finish.transform.position);

        // add this to the List of connections 
        connectorsList.Add(theConnection);

        // just a debug printout of connections - nothing more
        //		for(int i = 0; i < connectorsList.Count; i++)
        //        {
        //			Debug.Log("Connection #" + i + " is :" + connectorsList[i].start.name + connectorsList[i].start.GetInstanceID() + " to " 
        //				+ connectorsList[i].finish.name + connectorsList[i].finish.GetInstanceID() + " of type " + connectorsList[i].connectionType);
        //        }

        // Add score
        gameStatusTextManager.AddScore(20); 

        // we made a new connection - let's check if we've won1
        CheckForWinningConnection();
    }

	// adds a box collider to a line https://github.com/tejas123/add-collider-to-line-renderer-unity/blob/master/Assets/DrawPhysicsLine.cs
	private void AddColliderToLine(LineRenderer line, Vector3 startPos, Vector3 endPos)
	{
		BoxCollider col = new GameObject("Collider").AddComponent<BoxCollider> ();
		col.transform.parent = line.transform; // Collider is added as child object of line
		float lineLength = Vector3.Distance (startPos, endPos); // length of line
		col.size = new Vector3 (lineLength, 1f, 1f); // size of collider is set where X is length of line, Y is width of line, Z will be set as per requirement
		Vector3 midPoint = (startPos + endPos)/2;
		col.transform.position = midPoint; // setting position of collider object
		// Following lines calculate the angle between startPos and endPos
		float angle = (Mathf.Abs (startPos.y - endPos.y) / Mathf.Abs (startPos.x - endPos.x));
		if((startPos.y<endPos.y && startPos.x>endPos.x) || (endPos.y<startPos.y && endPos.x>startPos.x))
		{
			angle*=-1;
		}
		angle = Mathf.Rad2Deg * Mathf.Atan (angle);
		col.transform.Rotate (0, 0, angle);

	}

    // deletes a link
    public void DeleteConnection(ConnectionInfo theLink)
    {
		// Remove this link from the list of connections
    	connectorsList.Remove(theLink);

		// Remove this link game object (which in turn removes the graphical representation of this link)
    	Destroy(theLink.gameObject);
    }


	public bool CheckForExistingConnection(Connectable start, Connectable finish)
	{

		// Scan down the master connection list
		// If we find a list element where the unique instance ID's of both start and finish match the ones in the list then this connection exists already
		for(int i = 0; i < connectorsList.Count; i++)
        {
			if(connectorsList[i].start.GetInstanceID() == start.GetInstanceID() && connectorsList[i].finish.GetInstanceID() == finish.GetInstanceID())
			{
				return true;
			}
        }

        return false;

	}

	// depth first search recursive algo
	public void CheckForWinningConnection()
	{
		// the link that emanates from the starting object
		int startLinkIdx = -1;
		ConnectionInfo startLink = null;

		// find start obj link
		for(int i = 0; i < connectorsList.Count; i++)
        {	
        	// if this is a start obj
        	if(connectorsList[i].start.isStartObject)
        	{
        		// save this index
				startLinkIdx = i;
        	}

			// prepare all links for DFS traversal by initializing traversed to false
			connectorsList[i].linkTraversed = false;
        }

        // now that we have the starting link
        // lets traverse forward and see if we hit an ending object
		if(startLinkIdx != -1)
        {
			// get reference to the link which has the start object as it's first
			startLink = connectorsList[startLinkIdx];

			// we're visiting this link so set flag that we've looked at this
			startLink.linkTraversed = true;

			// check to see if this is a link to a finish object - if it is we win!
			if(startLink.finish.isFinishObject)
    		{
    			// DO WIN FUNCTION
    			Debug.Log("=========== YOU WIN ============");
                //GameObject.Find("Status Text").GetComponent<Text>().text = "You have won!!! :-o";
                gameStatusTextManager.Victory();
    		}

			// this is the very first call of the recursive DFS (depth first search) call
			TraverseLinks(startLink);
        }
		
	}

	public void TraverseLinks(ConnectionInfo link)
	{
		for(int i = 0; i < connectorsList.Count; i++)
        {	
			// We are looking at this current link so we have visited it
			link.linkTraversed = true;

        	// compared the finish of the link passed in with the start of the link we are looking at in the list
			// if it's a match - we have a subsequent link (ie a neighbor)
			// Debug.Log("Comparing: Finish:" + link.finish.GetInstanceID() + " and Start:" + connectorsList[i].start.GetInstanceID());
			if(link.finish.GetInstanceID() == connectorsList[i].start.GetInstanceID())
        	{
				// finish point of link passed in matches start point of link we are looking at
        		//Debug.Log("MATCH FOUND");

        		// check to see if this is a link to a finish object - if it is we win!
        		if(connectorsList[i].finish.isFinishObject)
        		{
        			// DO WIN FUNCTION
        			Debug.Log("=========== YOU WIN ============");
                    //GameObject.Find("Status Text").GetComponent<Text>().text = "You have won!!! :-o";
                    gameStatusTextManager.Victory();
        		}

        		// if it's not - let's traverse further down this link
				// only if we have not visited it yet
				if(connectorsList[i].linkTraversed == false)
				{
					TraverseLinks(connectorsList[i]);
				}
        		
        	}
        }



	}



}
