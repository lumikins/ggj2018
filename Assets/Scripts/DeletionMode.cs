﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeletionMode : MonoBehaviour {

	public bool deletionMode = false;

	void Start()
	{
		GameObject.Find("DeleteButtonText").GetComponent<Text>().text = "Deletion Mode: OFF";
	}

	// swaps false to true and vice versa
	public void ToggleDeletionMode()
	{
		if(deletionMode == false)
		{
			GameObject.Find("DeleteButtonText").GetComponent<Text>().text = "Deletion Mode: ON";
			deletionMode = true;
		}
		else
		{
			deletionMode = false;
			GameObject.Find("DeleteButtonText").GetComponent<Text>().text = "Deletion Mode: OFF";
		}
	}

}
